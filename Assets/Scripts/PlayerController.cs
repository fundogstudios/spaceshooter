﻿using System.Collections;
using UnityEngine;

public class PlayerController : MonoBehaviour 
{
	// Player
	public GameObject playerExplosion;

	// Movement
	public Movement movement;
	public Boundary boundry;

	// Shot
	public GameObject shot;
	public Transform shotSpawn;
	public float fireRate;


	private GameController gameController;
	private float nextFire = 0.0f;
	private Rigidbody rb;

	void Start() 
	{
		rb = GetComponent<Rigidbody>();

		GameObject gameControllerObject = GameObject.FindWithTag("GameController");
		gameController = gameControllerObject != null ? gameControllerObject.GetComponent<GameController>() : null;

		if (gameController == null) {
			Debug.Log("Cannot find 'GameController' script");
		}
	}

	void FixedUpdate()
	{
		float moveHorizontal = Input.GetAxis ("Horizontal");
		float moveVertical = Input.GetAxis ("Vertical");
		
		Vector3 nextMove = new Vector3(moveHorizontal, 0.0f, moveVertical);
		
		rb.velocity = nextMove * movement.speed;
		rb.position = new Vector3(
			Mathf.Clamp(rb.position.x, boundry.xMin, boundry.xMax), 
			0.0f, 
			Mathf.Clamp(rb.position.z, boundry.zMin, boundry.zMax)
		);

		rb.rotation = Quaternion.Euler(0.0f, 0.0f, rb.velocity.x * -movement.tilt);
	}

	void Update()
	{
		if ((Input.GetButton("Fire1") || Input.GetKeyDown(KeyCode.Return)) && Time.time > nextFire) {
			nextFire = Time.time + fireRate;
			Instantiate(shot, shotSpawn.position, shotSpawn.rotation);
		}
	}

		void OnTriggerEnter(Collider other)
	{
		switch(other.tag)
		{
			case "EnemyBolt":
				HitByBolt(other);
				break;

			// Default case catches Boundry
			default:
				return;
		}
	}

	void HitByBolt(Collider bolt)
	{
		Destroy(bolt.gameObject);
		KillPlayer();
	}

	public void KillPlayer()
	{
		Instantiate(playerExplosion, transform.position, transform.rotation);
		Destroy(gameObject);
		gameController.GameOver();
	}
}