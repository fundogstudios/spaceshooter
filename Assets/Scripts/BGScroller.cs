﻿using System.Collections;
using UnityEngine;

public class BGScroller : MonoBehaviour {
	public float scrollSpeed;
	public float tileSizeZ;
	
	private Transform bgTransform;
	private Vector3 startPosition;

	void Start () 
	{
		bgTransform = GetComponent<Transform>();
		startPosition = bgTransform.position;
	}

	void Update () 
	{
		float newPosition = Mathf.Repeat(Time.time * scrollSpeed, tileSizeZ);
		bgTransform.position = startPosition + Vector3.forward * newPosition;
	}
}
