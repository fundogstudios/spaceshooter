using System.Collections;
using UnityEngine;

public class EnemyController : MonoBehaviour 
{
    public GameObject shot;
	public Transform shotSpawn;
    public GameObject enemyExplosion;
    public int scoreValue;
	public float fireRate;
	public float fireDelay;
    public EvasiveManeuver evasiveness;
    public Movement movement;

	private float nextFire = 0.0f;
    private GameController gameController;
    private float currentSpeed;
    private float targetManeuver;
    private Rigidbody rb;


	void Start() 
	{
        rb = GetComponent<Rigidbody>();
        rb.velocity = transform.forward * movement.speed;
        currentSpeed = rb.velocity.z;
        GameObject gameControllerObject = GameObject.FindWithTag("GameController");
		gameController = gameControllerObject != null ? gameControllerObject.GetComponent<GameController>() : null;

		if (gameController == null) {
			Debug.Log("Cannot find 'GameController' script");
		}

        InvokeRepeating("Fire", fireDelay, fireRate);
        StartCoroutine (Evade ());
	}

    void FixedUpdate()
    {
        float newManeuver = Mathf.MoveTowards(rb.velocity.x, targetManeuver, Time.deltaTime * evasiveness.smoothing);
        rb.velocity = new Vector3(newManeuver, 0.0f, currentSpeed);
        rb.position = new Vector3
        (
            Mathf.Clamp (rb.position.x, evasiveness.boundary.xMin, evasiveness.boundary.xMax),
            0.0f,
            Mathf.Clamp (rb.position.z, evasiveness.boundary.zMin, evasiveness.boundary.zMax)
        );

        rb.rotation = Quaternion.Euler(0.0f, 0.0f, rb.velocity.x * -movement.tilt);
    }
	void OnTriggerEnter(Collider other)
	{
		switch(other.tag)
		{
			case "Player":
				HitByPlayer(other);
				break;

            case "Bolt":
                HitByBolt(other);
                break;

			default:
				return;
		}
	}

    void Fire()
	{
		if (Time.time > nextFire) {
			nextFire = Time.time + fireRate;
			Instantiate(shot, shotSpawn.position, shotSpawn.rotation);
		}
	}

	private void HitByBolt(Collider bolt)
	{
		Destroy(bolt.gameObject);
        KillEnemy(true);
	}

	private void HitByPlayer(Collider player)
	{        
        var pc = player.gameObject.GetComponent<PlayerController>();

        KillEnemy(false);
        pc.KillPlayer();
	}

    private void KillEnemy(bool addScore)
    {
        Instantiate(enemyExplosion, transform.position, transform.rotation);
        Destroy(gameObject);
        if (addScore) 
        {
            gameController.AddScore(scoreValue);
        }
    }

    IEnumerator Evade()
    {
        yield return new WaitForSeconds(Random.Range (evasiveness.startWait.x, evasiveness.startWait.y));

        while (true)
        {
            targetManeuver = Random.Range(1, evasiveness.dodge) * -Mathf.Sign (transform.position.x);
            yield return new WaitForSeconds(Random.Range (evasiveness.maneuverTime.x, evasiveness.maneuverTime.y));
            targetManeuver = 0;
            yield return new WaitForSeconds(Random.Range (evasiveness.maneuverWait.x, evasiveness.maneuverWait.y));
        }
    }
}