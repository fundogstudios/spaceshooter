﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

[System.Serializable]
public class GameText 
{
    public string scoreTextPrefix, restartTextString, gameOverTextString;
}

public class GameController : MonoBehaviour 
{
    public GameObject[] asteroids;
    public GameObject purpleShip;
    public GameObject redShip;
    public Vector3 spawnValues;
    public int initialHazardCount;
    public float spawnWait;
    public float startWait;
    public float waveWait;
    public GUIText scoreText;
    public GUIText restartText;
    public GUIText gameOverText;
    public GameText gameText;

    private bool gameOver;
    private bool restart;
    private int score;
    private int wave;
    private int hazardsAllowed;
    private List<GameObject> hazards;

    void Start()
    {
        restart = false;
        gameOver = false;
        restartText.text = "";
        gameOverText.text = "";
        score = 0;
        hazardsAllowed = initialHazardCount;
        hazards = new List<GameObject>();
        wave = 0;
        
        for (var i = 0; i < asteroids.Length; i++)
        {
            hazards.Add(asteroids[i]);
        }

        UpdateScore();
        StartCoroutine(SpawnWaves());
    }

    void Update()
    {
        if (restart)
        {
            if (Input.GetKeyDown(KeyCode.R))
            {
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            }
        }
    }

    IEnumerator SpawnWaves()
    {
        yield return new WaitForSeconds(startWait);
        while (true) 
        {
            ManageHazards();
            for (int i = 0; i < hazardsAllowed; i++)
            {
                GameObject hazard = hazards[Random.Range(0, hazards.Count)];
                Vector3 spawnPosition = new Vector3(Random.Range(-spawnValues.x, spawnValues.x), spawnValues.y, spawnValues.z);
                Quaternion spawnRotation = Quaternion.identity;
                Instantiate(hazard, spawnPosition, spawnRotation);
                yield return new WaitForSeconds(spawnWait);
            }

            yield return new WaitForSeconds(waveWait);

            if (gameOver)
            {
                AllowRestart();
                break;
            }
        }
    }

    public void AddScore (int newScoreValue)
    {
        score += newScoreValue;
        UpdateScore();
    }
    void UpdateScore()
    {
        scoreText.text = gameText.scoreTextPrefix + score;
        scoreText.text += "\n";
        scoreText.text += "wave:  " + wave;
    }

    public void GameOver()
    {
        gameOverText.text = gameText.gameOverTextString;
        gameOver = true;
    }

    void AllowRestart()
    {
        restartText.text = gameText.restartTextString;
        restart = true;
    }

    void ManageHazards()
    {
        wave++;
        UpdateScore();

        if (wave == 3)
        {
            hazards.Add(purpleShip);
        }

        if (wave == 6)
        {
            hazards.Add(purpleShip);
        }

        if (wave > 8 && wave % 3 == 0)
        {
            hazards.Add(redShip);
        }

        if (wave % 4 == 0)
        {
            hazardsAllowed++;
        }
    }
}
