using System.Collections;
using UnityEngine;

public class AsteroidController : MonoBehaviour 
{
  	public Movement movement;
    public GameObject asteroidExplosion;
    public int scoreValue;

    private Rigidbody rb;
    private GameController gameController;

	void Start()
	{
		rb = GetComponent<Rigidbody>();
		rb.angularVelocity = Random.insideUnitSphere * movement.tumble;
        rb.velocity = transform.forward * movement.speed;

        GameObject gameControllerObject = GameObject.FindWithTag("GameController");
		gameController = gameControllerObject != null ? gameControllerObject.GetComponent<GameController>() : null;

		if (gameController == null) {
			Debug.Log("Cannot find 'GameController' script");
		}
	}

	void OnTriggerEnter(Collider other)
	{
		switch(other.tag)
		{
			case "Player":
				HitByPlayer(other);
				break;

            case "Bolt":
                HitByBolt(other);
                break;

			default:
				return;
		}
	}

    private void HitByBolt(Collider bolt)
	{
		Destroy(bolt.gameObject);
        KillAsteroid(true);
	}

	private void HitByPlayer(Collider player)
	{        
        var pc = player.gameObject.GetComponent<PlayerController>();

        KillAsteroid(false);
        pc.KillPlayer();
	}

    private void KillAsteroid(bool addScore)
    {
        Instantiate(asteroidExplosion, transform.position, transform.rotation);
        Destroy(gameObject);
        if (addScore) 
        {
            gameController.AddScore(scoreValue);
        }
    }
}
