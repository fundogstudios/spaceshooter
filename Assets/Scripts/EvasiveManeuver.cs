using UnityEngine;

[System.Serializable]
public class EvasiveManeuver
{
	public float dodge, smoothing;
    public Vector2 startWait, maneuverTime, maneuverWait;
    public Boundary boundary;
}